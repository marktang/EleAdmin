export default {
  name: 'EdMenu',
  props: {
    // 菜单数据
    items: {
      type: Array,
      default: () => []
    }
  },
  render (h) {
    const vm = this

    return h(
      'el-menu',
      {
        props: {
          ...vm.$attrs
        },
        on: vm.$listeners
      },
      this.renderItems(h, this.items)
    )
  },
  methods: {
    /**
     * Icon 绘制
     */
    renderIcon (h, icon = 'el-icon-menu') {
      return h('i', { attrs: { class: icon } })
    },
    renderTitle (h, label) {
      return h('span', { slot: 'title' }, label)
    },
    /**
     * 绘制一级菜单
     */
    renderSubItem (h, item) {
      // let icon = this.renderIcon(h, item.icon)
      return h('el-submenu', {
        props: {
          ...item,
          index: item.id + ''
        }
      }, [
        h('template', { slot: 'title' }, [
          this.renderIcon(h, item.icon),
          this.renderTitle(h, item.label)
        ]),
        ...this.renderItems(h, item.items)])
    },
    /**
     * 绘制子菜单
     */
    renderItems (h, items) {
      return items.reduce((acc, item) => {
        if (item.items) {
          acc.push(this.renderSubItem(h, item))
        } else {
          acc.push(this.renderMenuItem(h, item))
        }
        return acc
      }, [])
    },
    /**
     * 绘制菜单项
     */
    renderMenuItem (h, item) {
      const props = {
        ...item,
        index: item.id + ''
      }
      return h('el-menu-item', {
        props
      }, [
        this.renderIcon(h, item.icon),
        this.renderTitle(h, item.label)
      ])
    }

    // renderMenuItemGroup (h, item) {
    //   const options = {
    //     props: {
    //       title: item.label
    //     }
    //   }
    //   return this.renderMenu(h, 'el-menu-item-group', item.elements, options)
    // },
  }
}
