import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const Simple = {
  name: 'Black',
  definition: {},
  vars: {},
  event: {}
}

export default new Vuex.Store({
  state: {
    url: '/element.php/index/element/index',
    /**
     * 视图信息
     */
    views: {
    },

    $index: 0
  },

  getters: {
    index: state => {
      return state.$index++
    }
  },

  mutations: {
    /**
     * 设置视图参数
     */
    setVar (state, { layer, key, value }) {
      Vue.set(state.views[layer].vars, key, value)
    },
    /**
     * 建立视图
     */
    setupView (state, layer) {
      Vue.set(state.views, layer, Simple)
    },
    /**
     * 拆除视图
     */
    removeView (state, layer) {
      Vue.delete(state.views, layer)
    },
    /**
     * 设置视图
     */
    setView (state, { layer, name, definition = {}, event = {}, vars = {} }) {
      state.views[layer] = { name, definition, event, vars }
    },
    /**
     * 刷新视图
     */
    resetView (state, { layer, definition }) {
      Object.assign(state.views[layer].definition, definition)
    },
    /**
     * 清空视图
     */
    dropView (state, layer) {
      state.views[layer] = Simple
    }
  },
  actions: {
    /**
     * 重置界面
     */
    async reViewByUrl ({ commit }, { url, layer, value }) {
      // console.log('Simple set')
      commit('setView', { layer, Simple })
      const data = await Vue.$http.Get(url, value)
      // console.log('Component set')
      commit('setView', {
        layer: layer,
        ...data
      })
    },
    /**
     * 刷新界面
     */
    async reloadViewByUrl ({ commit }, { url, layer, value = {} }) {
      const definition = await Vue.$http.Get(url, value)
      commit('resetView', { layer, definition })
    }
  },
  modules: {
  }
})
